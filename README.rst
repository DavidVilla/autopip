**autopip** performs automatic module installation from the PyPI.

Do not worry about dependencies, just import.

That will works even when ``requests`` is not installed (of course, you need Internet
access):


.. sourcecode:: python

    from autopip import requests

    print "using requests"
    r = requests.get("http://ip.nfriedly.com/text")
    print("Your IP is: " + r.text)




.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
