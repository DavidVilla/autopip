#!/usr/bin/env python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
from types import ModuleType
import imp
import pip
from multiprocessing import Process
import site

sys.path.append(site.USER_SITE)


class ModuleWrapper(ModuleType):
    def __init__(self, module):
        self.module = module

    def __getattr__(self, key):
        retval = getattr(self.module, key, None)
        if retval is not None:
            return retval

        try:
            return __import__(key)
        except ImportError:
            self.pip_install(key)
            return self.load_module(key)

    def pip_install(self, name):
        def pip_install(module):
            cmd = 'install --user {}'.format(module)
            pip.main(cmd.split())

        print("autopip: installing module '{}'".format(name))

        p = Process(target=pip_install, args=(name,))
        p.start()
        p.join()

    def load_module(self, name):
        fp, pathname, description = imp.find_module(name)
        try:
            return imp.load_module(name, fp, pathname, description)
        finally:
            if fp:
                fp.close()


sys.modules[__name__] = ModuleWrapper(sys.modules[__name__])
